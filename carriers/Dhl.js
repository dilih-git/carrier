/**
 * Tracking number example:
 * 1000125&originCountryCode=NZ
 * 1791954522
 * JD000390000993069072
 * 8219242010
 */

const axios = require('axios');

const parseDate = (date) => {
    const parts = date.split('/');
    return new Date(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10));
}

module.exports = class Dhl {
    constructor() {
        this.apiKey = '2d4pkuUCJFubEyhjxWx13AHow0JSD19M';
    }

    track(packetCode) {
        return new Promise((resolve, reject) => {
            axios.get(`https://api-eu.dhl.com/track/shipments?trackingNumber=${packetCode}`, {
                headers: { 'DHL-API-Key': this.apiKey }
            }).then(({ data }) => {
                console.log(data.shipments[0].status.timestamp);
                var isDelivered = false;
                const notifications = { date: data.shipments[0].status.timestamp, message: data.shipments[0].status.remark };
                if (data.shipments[0].status.status === "delivered")
                {
                    isDelivered = true;
                }
                var packageStatus = data.status;
                return resolve({
                    packageId: data.shipments[0].id,
                    transporter: 'DHL',
                    transporterUrl: 'https://www.dhl.com',
                    transporterImage: 'https://www.cash.at/typo3temp/pics/e7c47d5cbf.png',
                    delivered: isDelivered,
                    estimatedDelivry: data.shipments[0].estimatedTimeOfDelivery,
                    notifications: notifications
                });
            }).catch(err => {
                console.log(err)
                reject('packet not found: DHL', err)
            });
        });
    }
}
