const axios = require('axios');

const parseDate = (date) => {
  const parts = date.split('/');
  return new Date(parseInt(parts[2], 10), parseInt(parts[1], 10) - 1, parseInt(parts[0], 10));
}

module.exports = class LaPoste {
  constructor() {
    this.apiKey = 'JJ5M8onDloXnU8ZEgzKLYmwKC4z/F66o1AaWoeWZeT3UVbZu4abJ5Y3nLQ0n1uy6';
  }

  track(packetCode) {
    return new Promise((resolve, reject) => {
      axios.get(`https://api.laposte.fr/suivi/v1/${packetCode}`, {
        headers: { 'X-Okapi-Key': this.apiKey }
      }).then(({ data }) => {
        const notifications = [{ date: parseDate(data.date), message: data.message }];
        return resolve({
          packageId: packetCode,
          transporter: 'laposte',
          transporterUrl: 'https://laposte.fr',
          transporterImage: 'https://laposte.fr/logo.png',
          delivered: data.status == 'LIVRE',
          estimatedDelivry: null,
          notifications: notifications
        });
      }).catch(err => reject('packet not found: laposte', err));
    });
  }
}
