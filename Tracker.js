const LaPoste = require('./carriers/Laposte');
const Dhl = require('./carriers/Dhl');

module.exports = class Tracker {

  constructor() {
    this.carriers = {
      laposte: new LaPoste(),
      dhl: new Dhl(),
    }
  }

  /**
   * @track(packetCode, options)
   * packetCode -
   * options - { carrier }
   */
  track(packetCode, options = {}) {
    const carrier = options.carrier || findCarrier(packetCode);
    if (!this.carriers[carrier])
      throw 'carrier not found';
    return this.carriers[carrier].track(packetCode);
  }

  findCarrier(packetCode) {
    // todo
  }
}
